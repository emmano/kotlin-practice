package me.emmano.kotlinpractice.presentation.model

data class GithubUserModel(val login: String, val avatarUrl: String)
