package me.emmano.kotlinpractice.presentation.presenter

import me.emmano.kotlinpractice.data.repository.GithubUserDataRpository
import me.emmano.kotlinpractice.domain.interactor.UseCase
import me.emmano.kotlinpractice.domain.model.GithubUser
import me.emmano.kotlinpractice.domain.repository.UserRepository
import me.emmano.kotlinpractice.presentation.mapper.GithubUserPresentationMapper
import me.emmano.kotlinpractice.presentation.model.GithubUserModel
import rx.Observable
import rx.Scheduler
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import me.emmano.kotlinpractice.data.GithubUser as GithubUserServer

class UserListPresenter(val userListView: UserListView) : UseCase<List<GithubUser>> {

  val userRepository: UserRepository by lazy { GithubUserDataRpository() }

  override val subscriber: Subscriber<List<GithubUser>> = object : Subscriber<List<GithubUser>>() {
    override fun onError(e: Throwable?) {
      e?.printStackTrace()
    }

    override fun onCompleted() {
    }

    override fun onNext(githubUsers: List<GithubUser>?) {
      githubUsers?.let { userListView.renderUserList(GithubUserPresentationMapper().translate(it)) }
    }
  }

  override val observable: Observable<List<GithubUser>> = userRepository.users()
  override val scheduler: Scheduler = AndroidSchedulers.mainThread()

  fun getUserList() {
    execute()
  }
}

interface UserListView {
  fun renderUserList(userModelCollection: Collection<GithubUserModel>)
}
