package me.emmano.kotlinpractice.presentation.view

import android.databinding.DataBindingComponent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import kotlinx.android.synthetic.activity_main.*
import me.emmano.kotlinpractice.R
import me.emmano.kotlinpractice.databinding.GithubUserRowViewBinding
import me.emmano.kotlinpractice.presentation.model.GithubUserModel
import me.emmano.kotlinpractice.presentation.presenter.UserListPresenter
import me.emmano.kotlinpractice.presentation.presenter.UserListView
import kotlin.collections.toList

@Suppress("SYNTHETIC_DEPRECATED_PACKAGE")
class MainActivity : AppCompatActivity(), UserListView {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    UserListPresenter(this).getUserList()
  }

  override fun renderUserList(userModelCollection: Collection<GithubUserModel>) {
    recycler_view.layoutManager = LinearLayoutManager(this)
    recycler_view.adapter = GithubUserAdapter(userModelCollection.toList())
  }
}

class GithubUserAdapter(val githubUserList: List<GithubUserModel>) : RecyclerView.Adapter<GithubUserViewHolder>() {

  val component = object : DataBindingComponent {
    override fun getImageViewBinderAdapter(): ImageViewBinderAdapter? = object : ImageViewBinderAdapter {}
  }

  override fun onBindViewHolder(holder: GithubUserViewHolder, position: Int) = holder.bindTo(
      githubUserList[position])

  override fun getItemCount(): Int = githubUserList.size

  override fun onCreateViewHolder(parent: ViewGroup?,
      viewType: Int): GithubUserViewHolder = GithubUserViewHolder.create(
      LayoutInflater.from(parent?.context), parent!!, component)
}

class GithubUserViewHolder(val rowViewBinding: GithubUserRowViewBinding) : RecyclerView.ViewHolder(
    rowViewBinding.root) {

  companion object {
    fun create(layoutInflater: LayoutInflater, parent: ViewGroup,
        dataBindingComponent: DataBindingComponent) = GithubUserViewHolder(
        GithubUserRowViewBinding.inflate(layoutInflater, parent, false, dataBindingComponent))
  }

  fun bindTo(githubUserModel: GithubUserModel) {
    rowViewBinding.user = githubUserModel
    rowViewBinding.executePendingBindings()
  }
}
