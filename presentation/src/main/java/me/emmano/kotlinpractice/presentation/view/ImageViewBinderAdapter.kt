package me.emmano.kotlinpractice.presentation.view

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.bumptech.glide.Glide


public interface ImageViewBinderAdapter {
  @BindingAdapter("android:src")
  fun setImageUrl(imageView: ImageView, imageUrl: String) {
    Glide.with(imageView.context).load(imageUrl).into(imageView)
  }
}