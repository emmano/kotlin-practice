package me.emmano.kotlinpractice.presentation.mapper

import me.emmano.kotlinpractice.domain.model.GithubUser
import me.emmano.kotlinpractice.presentation.model.GithubUserModel
import kotlin.collections.map

class GithubUserPresentationMapper {

  fun translate(
      domainGithubUsers: List<GithubUser>): List<GithubUserModel> = domainGithubUsers.map {
    GithubUserModel(it.login, it.avatarUrl)
  }

}