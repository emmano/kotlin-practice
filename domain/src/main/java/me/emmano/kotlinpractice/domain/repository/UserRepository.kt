package me.emmano.kotlinpractice.domain.repository

import me.emmano.kotlinpractice.domain.model.GithubUser
import rx.Observable

interface UserRepository {

  fun users(): Observable<List<GithubUser>>
}