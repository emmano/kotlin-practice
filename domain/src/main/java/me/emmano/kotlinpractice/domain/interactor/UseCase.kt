package me.emmano.kotlinpractice.domain.interactor

import rx.Observable
import rx.Scheduler
import rx.Subscriber
import rx.schedulers.Schedulers

public interface UseCase<T> {

  val subscriber: Subscriber<T>
  val observable: Observable<T>
  val scheduler: Scheduler

  fun execute() {
    observable
        .subscribeOn(Schedulers.io())
        .observeOn(scheduler)
        .subscribe(subscriber)
  }
}