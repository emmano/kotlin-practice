package me.emmano.kotlinpractice.domain.model

data class GithubUser(val login: String, val avatarUrl: String)