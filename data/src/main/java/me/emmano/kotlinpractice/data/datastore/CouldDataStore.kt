package me.emmano.kotlinpractice.data.datastore

import me.emmano.kotlinpractice.data.GithubService
import me.emmano.kotlinpractice.data.GithubUser
import retrofit.GsonConverterFactory
import retrofit.Retrofit
import retrofit.RxJavaCallAdapterFactory
import rx.Observable

class CloudUserDataStore : UserDataStore {
  val retrofit: Retrofit by lazy {
    Retrofit.Builder().baseUrl("https://api.github.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
        .build()
  }

  override fun getUserList(): Observable<List<GithubUser>> = retrofit.create(
      GithubService::class.java).getUsers()

}