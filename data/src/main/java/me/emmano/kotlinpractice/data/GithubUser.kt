package me.emmano.kotlinpractice.data

import com.google.gson.annotations.SerializedName

data class GithubUser(val login: String, @SerializedName("avatar_url") val userAvatar: String)