package me.emmano.kotlinpractice.data

import retrofit.http.GET
import rx.Observable

interface GithubService {

  @GET("users/emmano/followers")
  fun getUsers(): Observable<List<GithubUser>>
}
