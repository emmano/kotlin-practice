package me.emmano.kotlinpractice.data.repository

import me.emmano.kotlinpractice.data.datastore.CloudUserDataStore
import me.emmano.kotlinpractice.data.datastore.UserDataStore
import me.emmano.kotlinpractice.domain.model.GithubUser
import me.emmano.kotlinpractice.domain.repository.UserRepository
import rx.Observable
import kotlin.collections.map

class GithubUserDataRpository : UserRepository {

  val userDataStore: UserDataStore = CloudUserDataStore()

  override fun users(): Observable<List<GithubUser>> = userDataStore.getUserList()
      .map { it.map { GithubUser(it.login, it.userAvatar) } }
}