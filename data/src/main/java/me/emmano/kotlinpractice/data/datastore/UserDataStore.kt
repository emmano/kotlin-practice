package me.emmano.kotlinpractice.data.datastore

import me.emmano.kotlinpractice.data.GithubUser
import rx.Observable

interface UserDataStore {

  fun getUserList(): Observable<List<GithubUser>>
}